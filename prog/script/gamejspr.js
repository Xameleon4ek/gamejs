/**
 * Created by Администратор on 01.12.2016.
 */

var game = new Phaser.Game(980, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update})
var image;

function preload() {
    game.load.image('PLPQ.png','img/PLPQ.png');
}

function create() {
    image= game.add.sprite(game.world.right, game.world.right, 'PLPQ.png');
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.enable(image, Phaser.Physics.ARCADE)
}

function update() {
    image.body.velocity.y = 100;
}
